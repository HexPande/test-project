<?php

namespace App\Services\ExternalService;

use Illuminate\Support\Facades\RateLimiter;
use LogicException;

final class ExternalService
{
    /**
     * Кол-во сообщений в интервале
     */
    public const N = 10;

    /**
     * Заданный интервал (минуты)
     */
    public const P = 5;

    /**
     * Отправляет группу сообщений во внешний сервис
     *
     * @param Message[] $messages
     */
    public function batch(array $messages): void
    {
        foreach ($messages as $message) {
            $this->send($message);
        }
    }

    /**
     * Отправляет сообщение во внешний сервис
     *
     * @param Message $message
     */
    protected function send(Message $message): void
    {
        $executed = RateLimiter::attempt(
            'message2external',
            self::N,
            function () use ($message) {
                return true;
            },
            self::P * 60
        );

        if (!$executed) {
            throw new LogicException('Too many messages sent!');
        }
    }
}
