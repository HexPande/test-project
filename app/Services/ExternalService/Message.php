<?php

namespace App\Services\ExternalService;

final class Message
{
    /** @var int */
    protected $id;

    /** @var string */
    protected $text;

    /**
     * @param int $id
     * @param string $text
     */
    public function __construct(int $id, string $text)
    {
        $this->id = $id;
        $this->text = $text;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function text(): string
    {
        return $this->text;
    }
}
