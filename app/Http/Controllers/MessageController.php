<?php

namespace App\Http\Controllers;

use App\Actions\SendMessagesAction;
use App\Http\Requests\SendMessageRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

final class MessageController extends Controller
{
    /**
     * Отправить коллекцию сообщений
     *
     * @param SendMessageRequest $request
     * @return JsonResponse
     */
    public function send(SendMessageRequest $request): JsonResponse
    {
        app(SendMessagesAction::class)->run($request);

        return new JsonResponse(['message' => 'OK']);
    }
}
