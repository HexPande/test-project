<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class SendMessageRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'messages' => ['array'],
            'messages.*.id' => ['required', 'int'],
            'messages.*.text' => ['required', 'string', 'max:2048'],
        ];
    }
}
