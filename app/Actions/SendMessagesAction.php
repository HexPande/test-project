<?php

namespace App\Actions;

use App\Http\Requests\SendMessageRequest;
use App\Services\ExternalService\ExternalService;

final class SendMessagesAction
{
    /** @var ExternalService */
    protected $service;

    public function __construct(ExternalService $service)
    {
        $this->service = $service;
    }

    public function run(SendMessageRequest $request): void
    {
        // ExternalService::N - максимальное кол-во сообщений в интервале
        // ExternalService::P - интервал для максимального кол-ва сообщений
        // $this->service->batch($messages); - пример отправки сообщений
    }
}
